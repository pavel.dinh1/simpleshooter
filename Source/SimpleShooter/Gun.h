// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Gun.generated.h"

UCLASS()
class SIMPLESHOOTER_API AGun : public AActor
{
	GENERATED_BODY()

private:
	UPROPERTY(VisibleAnywhere)
		USceneComponent* Root;

	UPROPERTY(VisibleAnywhere)
		USkeletalMeshComponent* GunMesh;
	
	UPROPERTY(EditDefaultsOnly, Category = Effects)
		UParticleSystem* MuzzleFlash;

	UPROPERTY(EditDefaultsOnly, Category = Effects)
		UParticleSystem* BulletImpact;

	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundBase* MuzzleSound;

	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundBase* BulletImpactSound;

	UPROPERTY(EditDefaultsOnly)
		float MaxRange = 10000.f;

	UPROPERTY(EditDefaultsOnly)
		float Damage = 10.f;

	bool GunTrace(FHitResult& Hit, FVector& ShotDirection);

	AController* GetOwnerController() const;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Sets default values for this actor's properties
	AGun();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void PullTrigger();
};
