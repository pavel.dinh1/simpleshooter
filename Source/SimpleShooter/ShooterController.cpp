// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterController.h"
#include "Blueprint/UserWidget.h"

void AShooterController::BeginPlay()
{
	Super::BeginPlay();

	HUDScreen = CreateWidget(this, HUDWidgetClass);
	if (HUDWidgetClass)
	{
		HUDScreen->AddToViewport();
	}
}

void AShooterController::GameHasEnded(class AActor* EndGameFocus, bool bIsWinner)
{
	Super::GameHasEnded(EndGameFocus, bIsWinner);

	if (HUDScreen) { HUDScreen->RemoveFromParent(); }

	if(bIsWinner)
	{
		UUserWidget* WinScreen = CreateWidget(this, WinWidgetClass);
		if (WinWidgetClass)
		{
			WinScreen->AddToViewport();
		}
	}
	else
	{
		UUserWidget* LoseScreen = CreateWidget(this, LoseWidgetClass);
		if (LoseScreen)
		{
			LoseScreen->AddToViewport();
		}
	}
	
	GetWorldTimerManager().SetTimer(Restart_TimerHandle, this, &APlayerController::RestartLevel, RestartDelay);
}