// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "ShooterController.generated.h"

/**
 * 
 */
UCLASS()
class SIMPLESHOOTER_API AShooterController : public APlayerController
{
	GENERATED_BODY()

private:
	FTimerHandle Restart_TimerHandle;

	UPROPERTY(EditAnywhere)
		float RestartDelay = 5.f;

	UPROPERTY(EditAnywhere)
		TSubclassOf<UUserWidget> LoseWidgetClass;
	UPROPERTY(EditAnywhere)
		TSubclassOf<UUserWidget> WinWidgetClass;
	UPROPERTY(EditAnywhere)
		TSubclassOf<UUserWidget> HUDWidgetClass;
	
	UPROPERTY()
	UUserWidget* HUDScreen = nullptr;

protected:
	virtual void BeginPlay() override;
public:
	virtual void GameHasEnded(class AActor* EndGameFocus, bool bIsWinner) override;
};